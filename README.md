# Openstack

1. Change the vars file, playbooks and inventory as needed  
2. Move the cirros and rhel images to glanceimages folder  (cirros is in the files folder; download rhel from rhn)
3. Run the playbook - ansible-playbook -i inventory/hosts postdeploy_overcloud.yml  

The following are executing and can be modified.  Add more testenvironment files to create isolated projects.  
- flavors  
- images  
- network  
- overcloud  
- testenvironment  

